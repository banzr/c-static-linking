# c-static-linking

Tutorial on static linking

```
## Compile Application
$  $CC -c -o app.o app.c

## Compile Static library
$  $CC -c -o mylib.o -fPIC -static lib.c

## Compress Static Library
$  ar -rcs mylib.a mylib.o

## Link Application to Static Library
$  $CC app.o -L -l:mylib.a -o CompoundApp

## Run Linked Application
$  ./CompoundApp

## Observe output
--> Say *phrase*
```
