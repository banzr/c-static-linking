#include "lib.h"
#include <stdio.h>

// Appends string 'what' and writes to said
int say(const char* what, char* said) {

  sprintf(said, "Say %s\n", what);

  // For debugging
  printf("Say %s\n", what);

  return 0;
}
